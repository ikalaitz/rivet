BEGIN PLOT /CRYSTAL_BALL_1991_I315873/d02-x01-y01
Title=Direct Photon Spectrum in $\Upsilon(1S)$ decay
XLabel=$x_\gamma$
YLabel=$1/N_{\gamma gg} \text{d}N^\gamma/\text{d}x_\gamma$
LogY=0
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1991_I315873/d02-x01-y02
Title=Direct Photon Spectrum in $\Upsilon(1S)$ decay
XLabel=$x_\gamma$
YLabel=$1/N_{\gamma gg} \text{d}N^\gamma/\text{d}x_\gamma$
LogY=0
END PLOT
