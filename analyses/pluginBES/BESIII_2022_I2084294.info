Name: BESIII_2022_I2084294
Year: 2022
Summary: Dalitz decay of $D^+_s\to K^+\pi^+\pi^-$
Experiment: BESIII
Collider: BEPC
InspireID: 2084294
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 -  Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2205.08844 [hep-ex]
RunInfo: Any process producing D_s+ mesons
Description:
  'Measurement of the mass distributions in the decay $D^+_s\to K^+\pi^+\pi^-$ by BES. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events using model in the paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022vaf
BibTeX: '@article{BESIII:2022vaf,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Amplitude analysis and branching fraction measurement of the decay $D_{s}^{+} \to K^+\pi^+\pi^-$}",
    eprint = "2205.08844",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "5",
    year = "2022"
}
'
