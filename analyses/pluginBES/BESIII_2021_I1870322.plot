BEGIN PLOT /BESIII_2021_I1870322/d01-x01-y01
Title=$\pi^+\pi^+$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-\eta$
XLabel=$m_{\pi^+\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870322/d01-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-\eta$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870322/d01-x01-y03
Title=$\pi^+\eta$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-\eta$
XLabel=$m_{\pi^+\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870322/d01-x01-y04
Title=$\pi^-\eta$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-\eta$
XLabel=$m_{\pi^-\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^-\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870322/d01-x01-y05
Title=$\pi^+\pi^+\pi^-$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-\eta$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870322/d01-x01-y06
Title=$\pi^+\pi^-\eta$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-\eta$
XLabel=$m_{\pi^+\pi^-\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
