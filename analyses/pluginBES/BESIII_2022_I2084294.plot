BEGIN PLOT /BESIII_2022_I2084294/d01-x01-y01
Title=$K^+\pi^+$ mass distribution in $D_s^+\to K^+ \pi^+\pi^-$
XLabel=$m_{K^+\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2084294/d01-x01-y02
Title=$K^+\pi^-$ mass distribution in $D_s^+\to K^+ \pi^+\pi^-$
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2084294/d01-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D_s^+\to K^+ \pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2084294/dalitz
Title=Dalitz plot for $D_s^+\to K^+ \pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^+\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\pi^-}/{\rm d}m^2_{\pi^+\pi^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
