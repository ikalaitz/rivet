BEGIN PLOT /BESIII_2020_I1771616/d01-x01-y01
Title=$K^+K^-$ mass distribution in $\psi(2S)\to K^+K^-\eta$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1771616/d01-x01-y02
Title=$K^+\eta$ mass distribution in $\psi(2S)\to K^+K^-\eta$
XLabel=$m_{K^+\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1771616/d01-x01-y03
Title=$K^-\eta$ mass distribution in $\psi(2S)\to K^+K^-\eta$
XLabel=$m_{K^-\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1771616/dalitz
Title=Dalitz plot for  $\psi(2S)\to K^+K^-\eta$
XLabel=$m^2_{K^+\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\eta}/{\rm d}m^2_{K^-\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
