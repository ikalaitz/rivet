BEGIN PLOT /BESIII_2021_I1837968/d01-x01-y01
Title=$pK^0_S$ mass distribution in $\Lambda_c^+\to p K^0_S\eta$
XLabel=$m_{pK^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{pK^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1837968/d01-x01-y02
Title=$K^0_S\eta$ mass distribution in $\Lambda_c^+\to p K^0_S\eta$
XLabel=$m_{K^0_S\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1837968/d01-x01-y03
Title=$p\eta$ mass distribution in $\Lambda_c^+\to p K^0_S\eta$
XLabel=$m_{p\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
