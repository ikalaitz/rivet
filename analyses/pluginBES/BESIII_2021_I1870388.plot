BEGIN PLOT /BESIII_2021_I1870388/d01-x01-y01
Title=$n\bar{\Lambda}^0$ mass distribution in $\chi_{c0}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m_{n\bar{\Lambda}^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{n\bar{\Lambda}^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870388/d01-x01-y02
Title=$nK^0_S$ mass distribution in $\chi_{c0}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m_{nK^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{nK^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870388/d01-x01-y03
Title=$K^0_S\bar{\Lambda}^0$ mass distribution in $\chi_{c0}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m_{K^0_S\bar{\Lambda}^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\bar{\Lambda}^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870388/dalitz_1
Title=Dalitz plot for $\chi_{c0}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m^2_{K^0_S\bar{\Lambda}^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{nK^0_S}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_S\bar{\Lambda}^0}/{\rm d}m^2_{nK^0_S}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2021_I1870388/d02-x01-y01
Title=$n\bar{\Lambda}^0$ mass distribution in $\chi_{c1}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m_{n\bar{\Lambda}^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{n\bar{\Lambda}^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870388/d02-x01-y02
Title=$nK^0_S$ mass distribution in $\chi_{c1}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m_{nK^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{nK^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870388/d02-x01-y03
Title=$K^0_S\bar{\Lambda}^0$ mass distribution in $\chi_{c1}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m_{K^0_S\bar{\Lambda}^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\bar{\Lambda}^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870388/dalitz_2
Title=Dalitz plot for $\chi_{c1}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m^2_{K^0_S\bar{\Lambda}^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{nK^0_S}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_S\bar{\Lambda}^0}/{\rm d}m^2_{nK^0_S}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2021_I1870388/d03-x01-y01
Title=$n\bar{\Lambda}^0$ mass distribution in $\chi_{c2}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m_{n\bar{\Lambda}^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{n\bar{\Lambda}^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870388/d03-x01-y02
Title=$nK^0_S$ mass distribution in $\chi_{c2}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m_{nK^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{nK^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870388/d03-x01-y03
Title=$K^0_S\bar{\Lambda}^0$ mass distribution in $\chi_{c2}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m_{K^0_S\bar{\Lambda}^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\bar{\Lambda}^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1870388/dalitz_3
Title=Dalitz plot for $\chi_{c2}\to nK^0_S\bar{\Lambda}^0$+c.c.
XLabel=$m^2_{K^0_S\bar{\Lambda}^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{nK^0_S}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_S\bar{\Lambda}^0}/{\rm d}m^2_{nK^0_S}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
