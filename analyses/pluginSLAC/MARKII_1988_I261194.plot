BEGIN PLOT /MARKII_1988_I261194/d01-x01-y01
Title=$\eta$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
END PLOT
