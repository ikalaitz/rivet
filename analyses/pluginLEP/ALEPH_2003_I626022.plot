BEGIN PLOT /ALEPH_2003_I626022/d01-x01-y01
XLabel=$|\cos\theta|$
YLabel=$\mathrm{d}\sigma(\gamma\gamma\to \pi^+\pi^-)/\mathrm{d}|\cos\theta|$ [nb]
LogY=0
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $2<\sqrt{s}<6$ GeV
END PLOT
BEGIN PLOT /ALEPH_2003_I626022/d02-x01-y01
XLabel=$|\cos\theta|$
YLabel=$\mathrm{d}\sigma(\gamma\gamma\to K^+K^-)/\mathrm{d}|\cos\theta|$ [nb]
LogY=0
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2<\sqrt{s}<4$ GeV
END PLOT
BEGIN PLOT /ALEPH_2003_I626022/d03-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \pi^+\pi^-)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /ALEPH_2003_I626022/d04-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to K^+K^-)$ [nb]
LogY=1
END PLOT
