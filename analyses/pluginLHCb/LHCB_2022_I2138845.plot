BEGIN PLOT /LHCB_2022_I2138845/d01-x01-y01
Title=$3\pi^+2\pi^-$ mass distribution in $B_c^+\to J/\psi 3\pi^+2\pi^-$
XLabel=$m_{3\pi^+2\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{3\pi^+2\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I2138845/d01-x01-y02
Title=$K^+K^-\pi^+\pi^+\pi^-$ mass distribution in $B_c^+\to J/\psi K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^+K^-\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I2138845/d01-x01-y03
Title=$4\pi^+3\pi^-$ mass distribution in $B_c^+\to J/\psi 4\pi^+3\pi^-$
XLabel=$m_{4\pi^+3\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{4\pi^+3\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I2138845/d02-x01-y01
Title=$\pi^+\pi^+\pi^-$ mass distribution in $B_c^+\to J/\psi 3\pi^+2\pi^-$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I2138845/d02-x01-y02
Title=$\pi^+\pi^+\pi^-$ mass distribution in $B_c^+\to \psi(2S) \pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I2138845/d03-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $B_c^+\to J/\psi 3\pi^+2\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I2138845/d03-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $B_c^+\to \psi(2S) \pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I2138845/d04-x01-y01
Title=$K^\pm\pi^\mp$ mass distribution in $B_c^+\to J/\psi K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I2138845/d04-x01-y02
Title=$K^+K^-$ mass distribution in $B_c^+\to J/\psi K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
XMax=1.05
END PLOT
