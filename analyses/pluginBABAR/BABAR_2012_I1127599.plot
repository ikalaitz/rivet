BEGIN PLOT /BABAR_2012_I1127599/d01-x01-y01
Title=$\bar{p}\pi^-$ mass  distribution in $B^-\to\Sigma_c^{++}\bar{p}\pi^-\pi^-$
XLabel=$m_{\bar{p}\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1127599/d02-x01-y01
Title=$\Sigma_c^{++}\pi^-$ mass  distribution in $B^-\to\Sigma_c^{++}\bar{p}\pi^-\pi^-$
XLabel=$m_{\Sigma_c^{++}\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma_c^{++}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1127599/d03-x01-y01
Title=$\Sigma_c^{++}\pi^-\pi^-$ mass  distribution in $B^-\to\Sigma_c^{++}\bar{p}\pi^-\pi^-$
XLabel=$m_{\Sigma_c^{++}\pi^-\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma_c^{++}\pi^-\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
