BEGIN PLOT /BABAR_2011_I901433/d01-x01-y0[1,4]
XLabel=$\chi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I901433/d01-x01-y0[2,5]
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I901433/d01-x01-y0[3,6]
XLabel=$|\cos\theta_{\phi\phi}|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}|\cos\theta_{\phi\phi}|$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I901433/d01-x01-y01
Title=$\chi$ distribution in $B\to\phi\phi K$ $2.94<m_{\phi\phi}<3.02\,$GeV
END PLOT
BEGIN PLOT /BABAR_2011_I901433/d01-x01-y02
Title=$\cos\theta$ distribution in $B\to\phi\phi K$ $2.94<m_{\phi\phi}<3.02\,$GeV
END PLOT
BEGIN PLOT /BABAR_2011_I901433/d01-x01-y03
Title=$|\cos\theta_{\phi\phi}|$ distribution in $B\to\phi\phi K$ $2.94<m_{\phi\phi}<3.02\,$GeV
END PLOT
BEGIN PLOT /BABAR_2011_I901433/d01-x01-y04
Title=$\chi$ distribution in $B\to\phi\phi K$ $m_{\phi\phi}<2.85\,$GeV
END PLOT
BEGIN PLOT /BABAR_2011_I901433/d01-x01-y05
Title=$\cos\theta$ distribution in $B\to\phi\phi K$ $m_{\phi\phi}<2.85\,$GeV
END PLOT
BEGIN PLOT /BABAR_2011_I901433/d01-x01-y06
Title=$|\cos\theta_{\phi\phi}|$  distribution in $B\to\phi\phi K$ $m_{\phi\phi}<2.85\,$GeV
END PLOT
